import Adafruit_LSM303  
import math
import datetime as dt
import http.client

calibrateThreshold = 10
calibrateCounter = 20

motionThreshold = 50
motionCounter = 10

baseAccel = None

lastSend = int(dt.datetime.utcnow().timestamp())
throttle = 5 

def diff(a, b):
    d = (a[0] - b[0], a[1] - b[1], a[2] - b[2])
    return length(d)

def length(d):
    return math.sqrt(d[0] ** 2 + d[1] ** 2 + d[2] ** 2)

def readAccel():
    lsm = Adafruit_LSM303.LSM303()
    accel, mag = lsm.read()
    return accel

def calibrate():
    prev = readAccel()
    counter = 0;
    while counter < calibrateCounter:
        curr = readAccel()
        d = diff(prev, curr)
        
        if d < calibrateThreshold:
            counter = counter + 1
        else:
            counter = 0
        prev = curr
    global baseAccel
    baseAccel = prev
    print("Calibration complete. Using {} as a base".format(baseAccel))

def checkMotion():     
    current = readAccel()
    d = diff(baseAccel, current)
    if d > motionThreshold:
        return True
    return False

def sendMotionDetected():
    global lastSend
    now = int(dt.datetime.utcnow().timestamp())
    if now > lastSend + throttle:
        sendRequest()
        lastSend = now

def sendRequest():
    conn = http.client.HTTPConnection('0.0.0.0', 5000)
    conn.request("POST", "/motion")
    print("Sending motion request")

if __name__ == '__main__':
    calibrate()
    counter = 0

    while True:
        motionDetected = checkMotion()

        if motionDetected:
            counter = counter + 1
        else:
            counter = 0

        if counter > motionCounter:
            sendMotionDetected()
            counter = 0
