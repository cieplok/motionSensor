import datetime as dt
from flask import Flask, Response, render_template, jsonify

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

inactive_delay = 20.0

@app.route('/')
def hello_world():
    return render_template("index.html", timestamp=dt.datetime.utcnow().timestamp())

@app.route('/motion', methods=['POST'])
def motion():
    date = dt.datetime.utcnow()
    write_to_file(date)
    response = 'Motion: {}'.format(date)
    return Response(response=response, status=200)

@app.route('/status')
def status():
    last_motion = float(read_from_file())
    utc_now = dt.datetime.utcnow().timestamp()
    is_active = last_motion + inactive_delay > utc_now
    return jsonify(
        isActive = is_active, 
        lastMotion= int(utc_now) - int(last_motion)
    )

def write_to_file(date):
    f = open('worklog.txt', 'w')
    timestamp = str(date.timestamp())
    f.write(timestamp)
    pass

def read_from_file():
    f = open('worklog.txt', 'r')
    return f.readline()
