window.onload = function() {
    Notification.requestPermission();

    getIsActive();
    window.setInterval(getIsActive, 5000);
}

var previouslyActive = false;

function updateStatus(response) {
    var state = response.isActive ? 'active' : 'free';

    var main = document.getElementById('main');
    main.classList.remove('active', 'free');
    main.classList.add(state);

    document.getElementById('state').innerHTML = state;
    document.getElementById('motion').innerHTML = response.lastMotion;

    if (previouslyActive === true && response.isActive === false) {
        notifyMe();
    }
    previouslyActive = response.isActive;
}

function getIsActive() {
    var request = new XMLHttpRequest();
    request.open('GET', '/status');
    request.responseType = 'json';
    request.onload = function() {
        updateStatus(request.response);
    };
    request.send();
}

function notifyMe() {
    if (!("Notification" in window)) {
      return;
    }
    else if (Notification.permission === "granted") {
      var notification = new Notification("Table is now free!", {
          body: "Go play!"
      });
    }
  }